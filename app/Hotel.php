<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    //

    use SoftDeletes;

    protected $guarded = ['id'];


    public function getManagerIdAttribute($value){
        $manager = Manager::find($value);

        if($manager){
            return $manager->name;
        }

        return null;
    }

}
