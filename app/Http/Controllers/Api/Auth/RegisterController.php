<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    public function register(Request $data)
    {

        $validation = Validator::make($data->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if($validation->fails()){
            return response()->json($validation->getMessageBag(),400);
        }



/*
            $generate_tokens = array(
                'form_params'=>[
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'vb6oCUlWJ4tqD9nMKugMokZ2XO4mtBUJAczyOp8o',
                'username' => 'sanson@gm.com',
                'password' => 'sanson'
                ]
            );

            $request_url = 'http://ghd.w3msys.com/oauth/token';

            $request = json_encode($generate_tokens);

            $ch = curl_init($request_url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Accept: application/json'
            ));

            $result = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            if ($err) {
                return response()->json('error '.$err);
            } else {
                $res = json_decode($result);

                return $res;
            }*/
        /*$http = new Client();

        $response=$http->post('http://localhost:4000/oauth/token',[
            'form_params'=>[
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'vb6oCUlWJ4tqD9nMKugMokZ2XO4mtBUJAczyOp8o',
                'username' => 'sanson@gm.com',
                'password' => 'sanson'
            ],
        ]);

        return json_decode((string) $response->getBody(), true);*/

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
